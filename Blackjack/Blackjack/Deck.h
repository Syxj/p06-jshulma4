#ifndef Deck_h
#define Deck_h

#endif

#import <Foundation/Foundation.h>

@interface Deck : NSObject

@property (strong, nonatomic) NSMutableDictionary *deck;

-(id)initWithDeck;
-(id)initEmpty;

-(void)resetDeck;
-(void)emptyDeck;
-(NSDictionary *)hitDeck;
-(void)addCard:(NSDictionary *)card;

-(NSMutableDictionary *)getDeck;

@end
