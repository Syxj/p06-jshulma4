#import <math.h>
#include "Player.h"

@implementation Player

-(id)init
{
    self = [super init];
    if (self)
    {
        _hand = [[Deck alloc] initEmpty];
    }
    return self;
}

-(void)emptyHand
{
    [_hand emptyDeck];
}

-(void)addCardToHand:(NSDictionary *)card
{
    [_hand addCard:card];
}

-(int)cardValue:(NSNumber *) key
{
    float tmpVal = [key floatValue];
    tmpVal /= 4.0;
    int tmpIntVal = ceilf(tmpVal); //Use this to round up everything except multiples of four.
    tmpIntVal++;
    if(tmpIntVal == 14) tmpIntVal = 1;
    else if (tmpIntVal > 10) tmpIntVal = 10;
    return tmpIntVal;
}

-(int)handValue
{
    //Will probably start with Aces = 1, then will change to allow for 1 or 11;
    int retVal = 0;
    NSArray* keys = [[_hand getDeck] allKeys];
    for(int i = 0; i < [keys count]; i++)
    {
        retVal += [self cardValue:keys[i]];
    }
    return retVal;
}

-(void)dealerLogic:(Deck *) deck:(int)playerVal
{
    int handVal = [self handValue];
    bool keepGoing = YES;
    while(keepGoing)
    {
        if([self handValue] > playerVal) break;
        int numCardsLeft = 0;
        NSArray *allKeys = [[deck getDeck] allKeys];
        int deckSize = (int)[allKeys count];
        for(int i = 0; i < deckSize; i++)
        {
            if(handVal + [self cardValue:allKeys[i]] <= 21) numCardsLeft++;
        }
        if((numCardsLeft/deckSize) > .40)
        {
            NSDictionary * card = [deck hitDeck];
            [self addCardToHand:card];
        }
        else
        {
            keepGoing = NO;
        }
    }
}

-(bool)playerWin:(int)dealer
{
    return ([self handValue] > dealer) || (dealer > 21);
}

-(NSDictionary *)getHand
{
    return [_hand getDeck];
}

@end
