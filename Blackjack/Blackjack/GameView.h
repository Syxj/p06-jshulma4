//
//  GameView.h
//  Blackjack
//
//  Created by Jordan Shulman on 4/17/17.
//  Copyright � 2017 Jordan Shulman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Player.h"
#import "Deck.h"

@interface GameView : UIView {}

@property (strong, nonatomic) Deck* gameDeck;
@property (strong, nonatomic) Player* player;
@property (strong, nonatomic) Player* dealer;

@property (strong, nonatomic) IBOutlet UIButton* hitButton;
@property (strong, nonatomic) IBOutlet UIButton* stickButton;

@property (strong, nonatomic) IBOutlet UILabel* playerLabel;
@property (strong, nonatomic) IBOutlet UILabel* dealerLabel;

@property (strong, nonatomic) NSMutableArray* images;

-(IBAction)hit:(id)sender;
-(IBAction)stick:(id)sender;

-(void)gameOver:(bool) winner;

-(void)showCards:(Player *) player;
-(void)showCardsHelper:(Player *) player;

@end
