//
//  AppDelegate.h
//  Blackjack
//
//  Created by Jordan Shulman on 4/14/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

