//
//  GameView.m
//  Blackjack
//
//  Created by Jordan Shulman on 4/17/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

#import "GameView.h"

@implementation GameView

-(id)init
{
    self = [super init];
    if(self)
    {
        _gameDeck = [[Deck alloc] initWithDeck];
        _player = [[Player alloc] init];
        _dealer = [[Player alloc] init];
        NSDictionary *card = [_gameDeck hitDeck];
        [_player addCardToHand:card];
        card = [_gameDeck hitDeck];
        [_player addCardToHand:card];
        card = [_gameDeck hitDeck];
        [_dealer addCardToHand:card];
        card = [_gameDeck hitDeck];
        [_dealer addCardToHand:card];
        NSArray *allValues = [[_player getHand] allValues];
        NSLog(@"You have a %@ and %@", allValues[0], allValues[1]);
        for(NSNumber *keys in card)
        {
            NSLog(@"You see the dealer has one unknown card and a %@", [card objectForKey:keys]);
            [_dealerLabel setText:[NSString stringWithFormat:@"The dealer has a %@", [card objectForKey:keys]]];
        }
        [_playerLabel setText:[NSString stringWithFormat:@"Total Value: %d", [_player handValue]]];
        _images = [[NSMutableArray alloc] init];
        [self showCards:_player];
    }
    return self;
}

-(IBAction)hit:(id)sender
{
    NSDictionary *card = [_gameDeck hitDeck];
    [_player addCardToHand:card];
    int i = 0;
    for(NSNumber *keys in card)
    {
        i++;
        NSLog(@"%d", i);
        NSLog(@"Card drawn: %@", [card objectForKey:keys]);
    }
    int handVal = [_player handValue];
    NSLog(@"Score: %d", handVal);
    [_playerLabel setText:[NSString stringWithFormat:@"Total Value: %d", [_player handValue]]];
    [self showCardsHelper:_player];
    if(handVal > 21)
    {
        NSLog(@"Bust!");
        [self gameOver:NO];
    }
}

-(IBAction)stick:(id)sender
{
    [_dealer dealerLogic:_gameDeck:[_player handValue]];
    int dealerHand = [_dealer handValue];
    NSLog(@"Dealer's score: %d", dealerHand);
    if([_player playerWin:dealerHand])
    {
        NSLog(@"Congrats, you win!");
        [self gameOver:YES];
    }
    else
    {
        NSLog(@"Sorry, you lost. Please play again!");
        [self gameOver:NO];
    }
}

//Info on how to make a UIAlertController/Action found on stack overflow
-(void)gameOver:(bool)winner
{
    NSString* gameOverString;
    if(winner)
    {
        gameOverString = [NSString stringWithFormat:@"Congratulations, you win! The score was %d to %d. Would you like to play again?",[_player handValue], [_dealer handValue]];
    }
    else
    {
        gameOverString = [NSString stringWithFormat:@"Sorry, you lost. The score was %d to %d. Would you like to play again?", [_player handValue], [_dealer handValue]];
    }
    UIAlertController *gameOverPrompt = [UIAlertController alertControllerWithTitle:@"Game Over" message:gameOverString preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yesButton = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        for(int i = 0; i < [_images count]; i++)
        {
            [_images[i] removeFromSuperview];
        }
        [self init];
    }];
    UIAlertAction *noButton = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {[self gameOver:NO];}];
    
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while(topVC.presentedViewController)
    {
        topVC = topVC.presentedViewController;
    }
    
    [gameOverPrompt addAction:yesButton];
    [gameOverPrompt addAction:noButton];
    [topVC presentViewController:gameOverPrompt animated:YES completion:nil];
}

-(void)showCardsHelper:(Player *)player
{
    for(int i = 0; i < [_images count]; i++)
    {
        [_images[i] removeFromSuperview];
    }
    _images = [[NSMutableArray alloc] init];
    [self showCards:player];
}

-(void)showCards:(Player *)player
{
    CGRect bounds = [self bounds];
    NSUInteger numCards = [[player getHand] count];
    NSArray* cards = [[player getHand] allValues];

    for(int i = 0; i < numCards; i++)
    {
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(bounds.size.width/(2*(i+1)), bounds.size.height - 60, 20, 30)];
        [image setImage:[UIImage imageNamed:cards[i]]];
        if([image image] == nil) [image setImage:[UIImage imageNamed:@"Back"]];
        [self addSubview:image];
        [_images addObject:image];
    }
    
}

@end
