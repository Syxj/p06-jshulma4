//
//  ViewController.m
//  Blackjack
//
//  Created by Jordan Shulman on 4/14/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _gameView = [_gameView init];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
