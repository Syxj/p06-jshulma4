#ifndef Player_h
#define Player_h

#endif

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface Player : NSObject

@property (strong, nonatomic) Deck *hand;

-(id)init;

-(void)emptyHand;
-(void)addCardToHand:(NSDictionary *)card;
-(int)cardValue:(NSNumber *)key;
-(int)handValue;

-(bool)playerWin:(int)dealer;

-(void)dealerLogic:(Deck *) deck:(int) playerVal;

-(NSDictionary *)getHand;

@end
