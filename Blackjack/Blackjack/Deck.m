#include "Deck.h"

@implementation Deck

-(id)initWithDeck
{
    self = [super init];
    if (self)
    {
        _deck =
        [@{
            [NSNumber numberWithInt:1] : @"Two of Hearts",
            [NSNumber numberWithInt:2] : @"Two of Diamonds",
            [NSNumber numberWithInt:3] : @"Two of Clubs",
            [NSNumber numberWithInt:4] : @"Two of Spades",
            [NSNumber numberWithInt:5] : @"Three of Hearts",
            [NSNumber numberWithInt:6] : @"Three of Diamonds",
            [NSNumber numberWithInt:7] : @"Three of Clubs",
            [NSNumber numberWithInt:8] : @"Three of Spades",
            [NSNumber numberWithInt:9] : @"Four of Hearts",
            [NSNumber numberWithInt:10] : @"Four of Diamonds",
            [NSNumber numberWithInt:11] : @"Four of Clubs",
            [NSNumber numberWithInt:12] : @"Four of Spades",
            [NSNumber numberWithInt:13] : @"Five of Hearts",
            [NSNumber numberWithInt:14] : @"Five of Diamonds",
            [NSNumber numberWithInt:15] : @"Five of Clubs",
            [NSNumber numberWithInt:16] : @"Five of Spades",
            [NSNumber numberWithInt:17] : @"Six of Hearts",
            [NSNumber numberWithInt:18] : @"Six of Diamonds",
            [NSNumber numberWithInt:19] : @"Six of Clubs",
            [NSNumber numberWithInt:20] : @"Six of Spades",
            [NSNumber numberWithInt:21] : @"Seven of Hearts",
            [NSNumber numberWithInt:22] : @"Seven of Diamonds",
            [NSNumber numberWithInt:23] : @"Seven of Clubs",
            [NSNumber numberWithInt:24] : @"Seven of Spades",
            [NSNumber numberWithInt:25] : @"Eight of Hearts",
            [NSNumber numberWithInt:26] : @"Eight of Diamonds",
            [NSNumber numberWithInt:27] : @"Eight of Clubs",
            [NSNumber numberWithInt:28] : @"Eight of Spades",
            [NSNumber numberWithInt:29] : @"Nine of Hearts",
            [NSNumber numberWithInt:30] : @"Nine of Diamonds",
            [NSNumber numberWithInt:31] : @"Nine of Clubs",
            [NSNumber numberWithInt:32] : @"Nine of Spades",
            [NSNumber numberWithInt:33] : @"Ten of Hearts",
            [NSNumber numberWithInt:34] : @"Ten of Diamonds",
            [NSNumber numberWithInt:35] : @"Ten of Clubs",
            [NSNumber numberWithInt:36] : @"Ten of Spades",
            [NSNumber numberWithInt:37] : @"Jack of Hearts",
            [NSNumber numberWithInt:38] : @"Jack of Diamonds",
            [NSNumber numberWithInt:39] : @"Jack of Clubs",
            [NSNumber numberWithInt:40] : @"Jack of Spades",
            [NSNumber numberWithInt:41] : @"Queen of Hearts",
            [NSNumber numberWithInt:42] : @"Queen of Diamonds",
            [NSNumber numberWithInt:43] : @"Queen of Clubs",
            [NSNumber numberWithInt:44] : @"Queen of Spades",
            [NSNumber numberWithInt:45] : @"King of Hearts",
            [NSNumber numberWithInt:46] : @"King of Diamonds",
            [NSNumber numberWithInt:47] : @"King of Clubs",
            [NSNumber numberWithInt:48] : @"King of Spades",
            [NSNumber numberWithInt:49] : @"Ace of Hearts",
            [NSNumber numberWithInt:50] : @"Ace of Diamonds",
            [NSNumber numberWithInt:51] : @"Ace of Clubs",
            [NSNumber numberWithInt:52] : @"Ace of Spades",
        } mutableCopy];
    }
    return self;
}

-(id)initEmpty
{
    self = [super init];
    if (self)
    {
        _deck = [@{} mutableCopy];
    }
    return self;
}

-(void)resetDeck
{
_deck =
    [@{
                             [NSNumber numberWithInt:1] : @"Two of Hearts",
                             [NSNumber numberWithInt:2] : @"Two of Diamonds",
                             [NSNumber numberWithInt:3] : @"Two of Clubs",
                             [NSNumber numberWithInt:4] : @"Two of Spades",
                             [NSNumber numberWithInt:5] : @"Three of Hearts",
                             [NSNumber numberWithInt:6] : @"Three of Diamonds",
                             [NSNumber numberWithInt:7] : @"Three of Clubs",
                             [NSNumber numberWithInt:8] : @"Three of Spades",
                             [NSNumber numberWithInt:9] : @"Four of Hearts",
                             [NSNumber numberWithInt:10] : @"Four of Diamonds",
                             [NSNumber numberWithInt:11] : @"Four of Clubs",
                             [NSNumber numberWithInt:12] : @"Four of Spades",
                             [NSNumber numberWithInt:13] : @"Five of Hearts",
                             [NSNumber numberWithInt:14] : @"Five of Diamonds",
                             [NSNumber numberWithInt:15] : @"Five of Clubs",
                             [NSNumber numberWithInt:16] : @"Five of Spades",
                             [NSNumber numberWithInt:17] : @"Six of Hearts",
                             [NSNumber numberWithInt:18] : @"Six of Diamonds",
                             [NSNumber numberWithInt:19] : @"Six of Clubs",
                             [NSNumber numberWithInt:20] : @"Six of Spades",
                             [NSNumber numberWithInt:21] : @"Seven of Hearts",
                             [NSNumber numberWithInt:22] : @"Seven of Diamonds",
                             [NSNumber numberWithInt:23] : @"Seven of Clubs",
                             [NSNumber numberWithInt:24] : @"Seven of Spades",
                             [NSNumber numberWithInt:25] : @"Eight of Hearts",
                             [NSNumber numberWithInt:26] : @"Eight of Diamonds",
                             [NSNumber numberWithInt:27] : @"Eight of Clubs",
                             [NSNumber numberWithInt:28] : @"Eight of Spades",
                             [NSNumber numberWithInt:29] : @"Nine of Hearts",
                             [NSNumber numberWithInt:30] : @"Nine of Diamonds",
                             [NSNumber numberWithInt:31] : @"Nine of Clubs",
                             [NSNumber numberWithInt:32] : @"Nine of Spades",
                             [NSNumber numberWithInt:33] : @"Ten of Hearts",
                             [NSNumber numberWithInt:34] : @"Ten of Diamonds",
                             [NSNumber numberWithInt:35] : @"Ten of Clubs",
                             [NSNumber numberWithInt:36] : @"Ten of Spades",
                             [NSNumber numberWithInt:37] : @"Jack of Hearts",
                             [NSNumber numberWithInt:38] : @"Jack of Diamonds",
                             [NSNumber numberWithInt:39] : @"Jack of Clubs",
                             [NSNumber numberWithInt:40] : @"Jack of Spades",
                             [NSNumber numberWithInt:41] : @"Queen of Hearts",
                             [NSNumber numberWithInt:42] : @"Queen of Diamonds",
                             [NSNumber numberWithInt:43] : @"Queen of Clubs",
                             [NSNumber numberWithInt:44] : @"Queen of Spades",
                             [NSNumber numberWithInt:45] : @"King of Hearts",
                             [NSNumber numberWithInt:46] : @"King of Diamonds",
                             [NSNumber numberWithInt:47] : @"King of Clubs",
                             [NSNumber numberWithInt:48] : @"King of Spades",
                             [NSNumber numberWithInt:49] : @"Ace of Hearts",
                             [NSNumber numberWithInt:50] : @"Ace of Diamonds",
                             [NSNumber numberWithInt:51] : @"Ace of Clubs",
                             [NSNumber numberWithInt:52] : @"Ace of Spades",
                            } mutableCopy];
}

-(void)emptyDeck
{
    _deck = [@{} mutableCopy];
}

-(void)addCard:(NSDictionary *)card
{
    NSArray *keys = [card allKeys];
    NSArray *cardNumber = [card allValues];
    [_deck setObject:cardNumber[0] forKey:keys[0]];
}

-(NSDictionary *)hitDeck
{
    int rVal = arc4random_uniform(52);
    NSNumber *randVal = [NSNumber numberWithInt:rVal];
    while(_deck[randVal] == nil)
    {
        rVal = arc4random_uniform(52);
        randVal = [NSNumber numberWithInt:rVal];
    }
    NSDictionary *retVal = @{randVal : _deck[randVal]};
    [_deck removeObjectForKey:randVal];
    return retVal;
}

-(NSMutableDictionary *)getDeck
{
    return _deck;
}

@end
